package org.example.backend.controller;

import org.example.backend.model.Movie;
import org.example.backend.services.MovieService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/movies")
public class MovieController {
//   csdf
    private final MovieService movieService;

    @Autowired
    public MovieController(MovieService movieService) {
        this.movieService = movieService;
    }

    @GetMapping
    public List<Movie> getAllMovies() {
        return movieService.getAllMovies();
    }

    @GetMapping("/{id}")
    public ResponseEntity<Movie> getMovieById(@PathVariable Long id) {
        return movieService.getMovieById(id)
                .map(ResponseEntity::ok)
                .orElse(ResponseEntity.notFound().build());
    }

    @PostMapping
    public Movie createMovie(@RequestBody Movie movie) {
        return movieService.saveMovie(movie);
    }

//    @PutMapping("/{id}")
//    public ResponseEntity<Movie> updateMovie(@PathVariable Long id, @RequestBody Movie movie) {
//        return movieService.getMovieById(id)
//                .map(existingMovie -> {
//                    existingMovie.setTitle(movie.getTitle());
//                    existingMovie.setGenre(movie.getGenre());
//                    existingMovie.setDirector(movie.getDirector());
//                    existingMovie.setYear(movie.getYear());
//                    existingMovie.setRating(movie.getRating());
//                    existingMovie.setDescription(movie.getDescription());
//                    existingMovie.setUrlToDB(movie.getUrlToDB());
//                    return ResponseEntity.ok(movieService.saveMovie(existingMovie));
//                })
//                .orElse(ResponseEntity.notFound().build());
//    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deleteMovie(@PathVariable Long id) {
        if (movieService.getMovieById(id).isPresent()) {
            movieService.deleteMovie(id);
            return ResponseEntity.ok().build();
        } else {
            return ResponseEntity.notFound().build();
        }
    }
}