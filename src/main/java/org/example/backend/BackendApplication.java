package org.example.backend;

import org.example.backend.model.Movie;
import org.example.backend.model.MovieExtension;
import org.example.backend.services.MovieService;
import org.example.backend.utils.MovieUtils;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@SpringBootApplication
public class BackendApplication implements CommandLineRunner {
//
    private final MovieService movieService;
    private final MovieUtils movieUtils;

    public BackendApplication(MovieService movieService, MovieUtils movieUtils) {
        this.movieService = movieService;
        this.movieUtils = movieUtils;
    }

    public static void main(String[] args) {
        SpringApplication.run(BackendApplication.class, args);
    }

    @Override
    public void run(String... args) throws IOException {

        movieService.saveMovie(Movie.builder().title("The Godfather").genre("Crime").rating(9.2).yearOfPublication(2022).description("sdfsdf").build());
        movieService.saveMovie(Movie.builder().title("The Shawshank Redemption").genre("Drama").rating(9.3).yearOfPublication(2022).description("fffsdfsd").build());
        movieService.saveMovie(Movie.builder().title("The Dark Knight").genre("Action").rating(9.0).yearOfPublication(2024).description("sdfsdfsdfsdfsdfsdfsdf").build());
        movieService.saveMovie(Movie.builder().title("The Lord of the Rings: The Return of the King").genre("Adventure").rating(8.9).yearOfPublication(2021).description("aaaaaaaaaaaaaaaaaaaa").build());

        Map<String, String> movieMap = movieUtils.readPathsOfAllFilesInGivenDirectory("C:\\Users\\cridder\\Videos")
                .stream()
                .filter(path -> !movieUtils.getNameOfMovie(path).equals("desktop"))
                .filter(movieUtils::isMovieFile)
                .collect(Collectors.toMap(movieUtils::getNameOfMovie, movieUtils::getTypeOfFile));

        movieMap.forEach((k, v) -> System.out.println("Name of Movie: " + k + ", Type of File: " + v));
    }
}
