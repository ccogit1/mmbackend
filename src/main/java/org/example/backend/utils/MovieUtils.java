package org.example.backend.utils;

import org.example.backend.model.MovieExtension;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

import static java.nio.file.Files.walk;

@Service
public class MovieUtils {
    public List<Path> readPathsOfAllFilesInGivenDirectory(String directoryPath) throws IOException {
        List<Path> paths = walk(Paths.get(directoryPath)).toList();
        return paths.stream()
                .filter(Files::isRegularFile)
                .toList();
//
    }

    public String getTypeOfFile(Path path) {
        int lastDotIndex = path.toString().lastIndexOf(".");
        if (lastDotIndex != -1) {
            return path.toString().substring(lastDotIndex + 1);
        }
        return "";
    }

    public String getNameOfMovie(Path path) {
        String moviePath = path.toString();
        int lastBackslashIndex = moviePath.lastIndexOf("\\");
        int lastDotIndex = moviePath.lastIndexOf(".");
        if (lastBackslashIndex != -1 && lastDotIndex != -1 && lastBackslashIndex < lastDotIndex) {
            return moviePath.substring(lastBackslashIndex + 1, lastDotIndex);
        }
        return "";
    }

    public boolean isMovieFile(Path path) {
        String typeOfFile = getTypeOfFile(path);
        for (MovieExtension movieExtension : MovieExtension.values()) {
            if (movieExtension.name().equalsIgnoreCase(typeOfFile)) {
                return true;
            }
        }
        return false;
    }
}
